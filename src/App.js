import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Grommet, Image, ResponsiveContext } from "grommet";
import Footer from "./organism/Footer";
import Header from "./organism/Header";
import Home from "./screen/Home";
import Portfolio from "./screen/Portfolio";
import Resume from "./screen/Resume";
import bg from "./assets/images/bg.svg";
import bgm from "./assets/images/bg-m.svg";
// import ParticlesBg from "particles-bg";
import "./App.css";

const theme = {
  global: {
    colors: {
      brand: "#228BE6",
    },
    font: {
      family: "Roboto",
      size: "18px",
      height: "20px",
    },
  },
};

function App() {
  return (
    <Grommet theme={theme} full>
      <ResponsiveContext.Consumer>
        {(size) => (
          <BrowserRouter>
            <Header />
            <Image
              src={size !== "small" ? bg : bgm}
              fit="contain"
              className="bg"
            />
            {/* <ParticlesBg
              background="#1a1a1a"
              color="#C2E8FF"
              num={size !== "small" ? 200 : 60}
              type="cobweb"
              bg={true}
            /> */}
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/portfolio" exact component={Portfolio} />
              <Route path="/resume" exact component={Resume} />
              <Route path="/clients" exact component={Resume} />
              {/* <Route component={Notfound} /> */}
            </Switch>
            <Footer />
          </BrowserRouter>
        )}
      </ResponsiveContext.Consumer>
    </Grommet>
  );
}

export default App;
