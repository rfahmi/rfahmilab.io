import React, { useState, useEffect } from "react";
import firebase from "../config/fb";
import { Box, ResponsiveContext, Image, Button, Carousel, Text } from "grommet";
import "react-alice-carousel/lib/alice-carousel.css";
import { CSSTransitionGroup } from "react-transition-group";
import { Next } from "grommet-icons";
import Ene from "../components/Ene/Lookback";
function Portfolio({ initialChild, ...props }) {
  const [portfolios, setPortfolios] = useState([]);

  useEffect(() => {
    firebase
      .firestore()
      .collection("portfolio")
      .onSnapshot(async (snap) => {
        const data = await snap.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        await setPortfolios(data);
      });
  }, []);

  return (
    <ResponsiveContext.Consumer>
      {(size) => (
        <Box pad="medium">
          <CSSTransitionGroup
            transitionName="slideTransition"
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnter={false}
            transitionLeave={false}
          >
            <Box gap="medium" direction="row" margin={{ right: "30%" }}>
              <Carousel controls="selectors" fill>
                {portfolios.map((i, index) => (
                  <Box direction="row">
                    <Box
                      flex
                      background="transparent"
                      style={{ height: "500px" }}
                    >
                      <Image key={index} src={i.pic} fit="contain" />
                    </Box>
                    <Box flex round="medium" pad="large">
                      <Box fill>
                        <h1>{i.name}</h1>
                        <Box direction="row" margin={{ bottom: "medium" }}>
                          {i.tech.map((t, index) => (
                            <Button
                              key={index}
                              primary
                              color="light-2"
                              margin={{ right: "4px" }}
                              size="small"
                              label={t}
                            />
                          ))}
                        </Box>
                        <p>{i.desc}</p>
                        <Text
                          size="small"
                          color="dark-3"
                        >{`- Dibuat tahun ${i.created}`}</Text>
                      </Box>
                      <Button
                        style={{ zIndex: 3 }}
                        href={i.url}
                        primary
                        size="large"
                        label="Check Now!"
                        icon={<Next />}
                        reverse
                      />
                    </Box>
                  </Box>
                ))}
              </Carousel>
            </Box>
          </CSSTransitionGroup>
          {size !== "small" && <Ene />}
        </Box>
      )}
    </ResponsiveContext.Consumer>
  );
}
export default Portfolio;
