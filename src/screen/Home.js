import React, { useState, useEffect } from "react";
import firebase from "../config/fb";
import { Box, Button, ResponsiveContext, Text } from "grommet";

import { FormNextLink } from "grommet-icons";
import Ene from "../components/Ene/Please";
import { CSSTransitionGroup } from "react-transition-group";

function Home(props) {
  const [skills, setSkills] = useState([]);

  useEffect(() => {
    firebase
      .firestore()
      .collection("skills")
      .onSnapshot(async (snap) => {
        const newSkill = await snap.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        await setSkills(newSkill);
      });
  }, []);

  return (
    <ResponsiveContext.Consumer>
      {(size) => (
        <Box pad="large" overflow="hidden">
          <CSSTransitionGroup
            transitionName="slideTransition"
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnter={false}
            transitionLeave={false}
          >
            <Box pad="large" direction="column">
              <Text size={size !== "small" ? "large" : "medium"}>
                I'm Fahmi,
              </Text>
              <Text
                size={size !== "small" ? "80px" : "50px"}
                weight="bold"
                color="#2c323f"
              >
                Software Developer
              </Text>
              <Text size={size !== "small" ? "large" : "medium"}>
                I create websites and apps with:
              </Text>
              <Box direction="row" margin={{ top: "large", bottom: "large" }}>
                {skills.map((skill) => (
                  <Button
                    key={skill.id}
                    primary
                    color="light-2"
                    margin={{ right: "4px" }}
                    size="small"
                    label={skill.lang}
                  />
                ))}
              </Box>
              <Box direction="row">
                <Button
                  primary
                  icon={<FormNextLink />}
                  size="large"
                  margin={{ right: "small" }}
                  label="See my works"
                  onClick={() => props.history.push("/portfolio")}
                />
              </Box>
            </Box>
          </CSSTransitionGroup>
          {size !== "small" && <Ene />}
        </Box>
      )}
    </ResponsiveContext.Consumer>
  );
}
export default Home;
