import React, { useState, useEffect } from "react";
import firebase from "../config/fb";
import {
  Box,
  ResponsiveContext,
  Text,
  Image,
  Heading,
  Stack,
  Paragraph,
} from "grommet";
import * as Icon from "grommet-icons";
import qr from "../assets/images/qrcode.png";
import { CSSTransitionGroup } from "react-transition-group";

function Resume() {
  const [profile, setProfile] = useState([]);

  useEffect(() => {
    firebase
      .firestore()
      .collection("profile")
      .onSnapshot(async (snap) => {
        const data = await snap.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        await setProfile(data);
      });
  }, []);

  return (
    <CSSTransitionGroup
      transitionName="slideTransition"
      transitionAppear={true}
      transitionAppearTimeout={500}
      transitionEnter={false}
      transitionLeave={false}
    >
      <ResponsiveContext.Consumer>
        {(size) => (
          <Box>
            {profile.map((i, index) => (
              <Box
                pad={{
                  top: "small",
                  bottom: "small",
                  left: "xlarge",
                  right: "xlarge",
                }}
                direction="column"
                flex
                overflow={{ horizontal: "hidden" }}
              >
                <Box margin={{ bottom: "medium" }}>
                  <Stack anchor="top-left">
                    <Box
                      elevation="large"
                      justify="start"
                      align="start"
                      pad="large"
                      background="linear-gradient(102.77deg, #865ED6 -9.18%, #18BAB9 209.09%)"
                      round="medium"
                      direction="row"
                    >
                      <Box flex>
                        <Heading level={2} color="#fff" weight="bold">
                          {i.name}
                        </Heading>
                        <Text size="medium" color="#fff" weight="normal">
                          {i.job}
                        </Text>
                        <Box direction="row" margin={{ top: "16px" }}>
                          <Icon.Location color="#fff" />
                          <Text
                            size="small"
                            color="#fff"
                            weight="normal"
                            margin={{ left: "16px" }}
                          >
                            {i.addr}
                          </Text>
                        </Box>
                        <Box direction="row">
                          <Icon.Phone color="#fff" />
                          <Text
                            size="small"
                            color="#fff"
                            weight="normal"
                            margin={{ left: "16px" }}
                          >
                            {i.phone}
                          </Text>
                        </Box>
                        <Box direction="row">
                          <Icon.Mail color="#fff" />
                          <Text
                            size="small"
                            color="#fff"
                            weight="normal"
                            margin={{ left: "16px" }}
                          >
                            {i.email}
                          </Text>
                        </Box>
                      </Box>
                      <Box align="end" justify="center" flex>
                        <Box height="small" width="small">
                          <Image src={qr} fit="contain" />
                        </Box>
                      </Box>
                    </Box>
                  </Stack>
                </Box>
                <Box
                  elevation="large"
                  justify="start"
                  align="start"
                  pad="large"
                  background="#fff"
                  round="medium"
                >
                  <Heading level={3}>Bio</Heading>
                  <Paragraph>{i.bio}</Paragraph>
                  <Heading level={3}>Experience</Heading>
                  <Paragraph>coming soon</Paragraph>
                  <Heading level={3}>Education</Heading>
                  <Paragraph>coming soon</Paragraph>
                </Box>
              </Box>
            ))}
          </Box>
        )}
      </ResponsiveContext.Consumer>
    </CSSTransitionGroup>
  );
}
export default Resume;
