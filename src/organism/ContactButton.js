import React from "react";
import { Box, Button, Text } from "grommet";
import { Mail, Chat } from "grommet-icons";

const ContactButton = (props) => (
  <>
    <Text
      size="medium"
      weight="bold"
      color="#2c323f"
      margin={{ bottom: "medium" }}
    >
      Let's Get In Touch
    </Text>
    <Box direction="row">
      <Button
        primary
        icon={<Mail />}
        size={props.btnSize}
        margin={{ right: "small" }}
        label="Email"
        onClick={() => {}}
      />
      <Button
        icon={<Chat />}
        size={props.btnSize}
        margin={{ right: "small" }}
        label="Chat"
        onClick={() => props.setShowSidebar()}
      />
    </Box>
  </>
);
export default ContactButton;
