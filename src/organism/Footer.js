import React from "react";
import { Box, Nav, Anchor, Text, Image, Footer } from "grommet";
import { Github, Linkedin, Instagram } from "grommet-icons";
import logo from "../assets/images/rf.svg";

const AppBar = (props) => (
  <Footer
    background="dark-1"
    style={{ position: "absolute", bottom: 0, width: "100%", zIndex: 0 }}
    pad={{ horizontal: "large", vertical: "small" }}
  >
    <Nav direction="row">
      <Anchor icon={<Instagram color="white" />} hoverIndicator />
      <Anchor icon={<Github color="white" />} hoverIndicator />
      <Anchor icon={<Linkedin color="white" />} hoverIndicator />
    </Nav>
    <Box direction="row" gap="small">
      <Image src={logo} alt="logo" fit="contain" />
      <Text alignSelf="center" size="small">
        rfahmi.com
      </Text>
    </Box>
  </Footer>
);
export default AppBar;
