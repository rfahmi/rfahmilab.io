import React from "react";
import { Link } from "react-router-dom";
import { Box, Heading, Nav, Anchor, Image, ResponsiveContext } from "grommet";
import logo from "../assets/images/rf.svg";
import { Menu } from "grommet-icons";

const Header = (props) => (
  <ResponsiveContext.Consumer>
    {(size) => (
      <Box
        tag="header"
        direction="row"
        align="center"
        justify="between"
        background="transparent"
        pad={{ left: "large", right: "large", vertical: "small" }}
        style={{ zIndex: "1", position: "sticky" }}
        {...props}
      >
        <Heading level="3" margin="none">
          <Box
            flex
            direction="row"
            justify="start"
            align="center"
            className="App-logo"
          >
            <Image src={logo} alt="logo" fit="contain" />
            Fahmi Rizalul
          </Box>
        </Heading>
        {size !== "small" ? (
          <Nav direction="row" pad="small">
            <Link to="/" style={{ textDecoration: "none", color: "#fff" }}>
              Home
            </Link>
            <Link
              to="/portfolio"
              style={{ textDecoration: "none", color: "#fff" }}
            >
              Portfolio
            </Link>
            <Link
              to="/resume"
              style={{ textDecoration: "none", color: "#fff" }}
            >
              Resume
            </Link>
            <Link
              to="/clients"
              style={{ textDecoration: "none", color: "#fff" }}
            >
              Clients
            </Link>
          </Nav>
        ) : (
          <Nav direction="row" pad="small">
            <Anchor color="white" icon={<Menu />} hoverIndicator />
          </Nav>
        )}
      </Box>
    )}
  </ResponsiveContext.Consumer>
);
export default Header;
