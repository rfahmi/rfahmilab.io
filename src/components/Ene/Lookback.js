import React, { useState } from "react";
import { Box, Image } from "grommet";
import img1 from "../../assets/images/ene/lookback.png";
import img2 from "../../assets/images/ene/describeNormal-1.png";
// import img3 from "../../assets/images/ene/describeNormal-2.png";

function Ene() {
  const [stateImg, setStateImg] = useState(img1);

  const tooglePosition = () => {
    if (stateImg === img1) {
      setStateImg(img2);
    } else {
      setStateImg(img1);
    }
  };
  // const toogleBlink = () => {
  //   if (stateImg === img2) {
  //     setStateImg(img3);
  //   } else if (stateImg === img3) {
  //     setStateImg(img2);
  //   }
  // };

  return (
    <>
      <Box className="ene-container">
        <Image
          src={stateImg}
          fit="contain"
          className="ene-img"
          onClick={() => tooglePosition()}
        />
      </Box>
    </>
  );
}
export default Ene;
