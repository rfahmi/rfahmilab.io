import React from "react";
import { Box, Image } from "grommet";
import im from "../../assets/images/ene/lookback.png";

function Ene(props) {
  return (
    <>
      <Box className="ene-container">
        <Image
          src={im}
          fit="contain"
          className="ene-img"
          onMouseEnter={() => console.warn("hoper")}
        />
      </Box>
    </>
  );
}
export default Ene;
