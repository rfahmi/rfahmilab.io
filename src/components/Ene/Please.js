import React, { useState } from "react";
import { Box, Image, TextInput, Button, Text } from "grommet";
import { FormDown, FormUp } from "grommet-icons";
import Fade from "react-reveal/Fade";
import { ApiAiClient } from "api-ai-javascript";
import Loading from "../Loading";

import img1 from "../../assets/images/ene/pleaseTalk-1.png";
// import img2 from "../../assets/images/ene/pleaseTalk-2.png";
import img3 from "../../assets/images/ene/pleaseNormal-1.png";
// import img4 from "../../assets/images/ene/pleaseNormal-2.png";

function Ene() {
  const [showInput, setShowInput] = useState(false);
  const [isTyping, setIsTyping] = useState(false);
  const [stateImg, setStateImg] = useState(img1);
  const [answer, setAnswer] = useState("Hai! Silahkan tanya sesuatu...");
  const [question, setQuestion] = useState("");

  const accessToken = "afc74fc5181e4594bbbef9290a02277d";
  const client = new ApiAiClient({ accessToken });
  // const toogleBlink = () => {
  //   if (stateImg === img1) {
  //     setStateImg(img2);
  //   } else if (stateImg === img2) {
  //     setStateImg(img1);
  //   } else if (stateImg === img3) {
  //     setStateImg(img4);
  //   } else if (stateImg === img4) {
  //     setStateImg(img3);
  //   }
  // };
  const setTalk = () => {
    setStateImg(img1);
  };
  const setStopTalk = () => {
    setStateImg(img3);
  };

  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     toogleBlink();
  //   }, 2000);
  //   return () => {
  //     clearInterval(interval);
  //   };
  // }, []);

  const toogleInput = () => {
    setStopTalk();
    setShowInput(!showInput);
  };

  const ask = () => {
    setTalk();
    setIsTyping(true);
    if (question !== "") {
      client.textRequest(question).then(onSuccess);
      function onSuccess(response) {
        // console.log(response);
        setIsTyping(false);
        setAnswer(response.result.fulfillment.speech);
      }
    } else {
      setAnswer("Coba ketik sesuatu");
      setIsTyping(false);
    }
    setQuestion("");
  };
  return (
    <>
      <Box className="ene-container">
        <Image src={stateImg} fit="contain" className="ene-img" />
        <Box
          direction="column"
          gap="medium"
          style={{
            position: "absolute",
            top: 50,
            left: -50,
            width: 200,
          }}
        >
          <Box
            pad="small"
            elevation="medium"
            hoverIndicator
            background="linear-gradient(102.77deg, #865ED6 -9.18%, #18BAB9 209.09%)"
            onClick={() => {
              toogleInput();
            }}
            round="small"
            style={{
              backgroundColor: "white",
              color: "#fff",
            }}
          >
            <div
              style={{
                position: "absolute",
                top: 20,
                right: -20,
                width: 0,
                height: 0,
                borderTop: "10px solid transparent",
                borderBottom: "10px solid transparent",
                borderLeft: "10px solid #528AC8",
                borderRight: "10px solid transparent",
              }}
            />
            {isTyping ? (
              <Loading type="cylon" color="#fff" />
            ) : (
              <Text size="small">{answer}</Text>
            )}
            <Box
              direction="row-reverse"
              style={{ fontSize: 11, color: "#eee" }}
            >
              {showInput ? (
                <FormUp color="light-5" />
              ) : (
                <FormDown color="light-5" />
              )}
            </Box>
          </Box>
          <Fade duration={300} top when={showInput}>
            <Box
              pad="small"
              elevation="medium"
              hoverIndicator
              background="white"
              round="small"
              align="end"
              style={{
                backgroundColor: "white",
                width: 200,
              }}
            >
              <TextInput
                id="text-input"
                size="small"
                placeholder="Tulis disini..."
                style={{ border: 0 }}
                value={question}
                onChange={(e) => {
                  setStopTalk();
                  setQuestion(e.target.value);
                }}
              />
              <Button
                onClick={() => ask()}
                size="small"
                label="Tanya"
                primary
              />
            </Box>
          </Fade>
        </Box>
      </Box>
    </>
  );
}
export default Ene;
